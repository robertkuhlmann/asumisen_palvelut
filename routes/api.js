const express = require('express');
const router = express();
const mail = require('../services/mail.js');

/*****
** SEND CONFIRMATION EMAIL TO CUSTOMER
*****/

router.post('/serviceRequestEmail', async (req, res) => {
  try{
    let status = await mail.send(req.body);
    res.send(status);
  }
  catch(err){
    console.error(err);
    res.status(500).send(err);
  }
});

router.post('/saunaEmail', async (req, res) => {
  try{
    let status = await mail.sendSauna(req.body);
    res.send(status);
  }
  catch(err){
    console.error(err);
    res.status(500).send(err);
  }
});

router.post('/laundryEmail', async (req, res) => {
  try{
    let status = await mail.sendLaundry(req.body);
    res.send(status);
  }
  catch(err){
    console.error(err);
    res.status(500).send(err);
  }
});

module.exports = router;