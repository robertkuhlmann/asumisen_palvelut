# Temporary layer for compiling TypeSript
FROM node:9.6.1-alpine as compiler
WORKDIR /tmp

RUN yarn global add @angular/cli 
COPY public/package.json .
RUN yarn

# TEMPORARY FIX TO FIREBASE / ANGULARFIRE2 CONFLICT. REMOVE!!
RUN sed -i '/FirebaseApp implements FBApp/ a automaticDataCollectionEnabled: boolean;' ./node_modules/angularfire2/firebase.app.module.d.ts

COPY public/ ./
RUN ng build

# Deployable container
FROM node:9.6.1-alpine
WORKDIR /opt/asumisen_palvelut

RUN apk add --no-cache tini
COPY package.json .
RUN yarn

COPY --from=compiler /tmp/dist ./public/dist
COPY routes ./routes
COPY credentials ./credentials
COPY services ./services
COPY server.js .

ENV PORT=8080
ENV ADDRESS=0.0.0.0
EXPOSE 8080

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node","server.js"]