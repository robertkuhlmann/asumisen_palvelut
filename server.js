const express = require('express');
const app = express();
const apiRouter = require('./routes/api');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use('/api', apiRouter);

// route everything else to index
app.use('/', express.static(__dirname + '/public/dist'));

app.get('/*', function(req, res){
  res.sendFile(__dirname + '/public/dist/index.html');
});

const address = process.env.ADDRESS || "0.0.0.0";
const port = process.env.PORT || 8080;
app.listen(port, address, () => {
  console.log(`app listening on ${address}:${port}`);
});