# asumisen_palvelut

Web service for managing properties.

---
## Requirements:
- docker ^18.x.x
- nodejs ^8.6.0
- Google Firebase project
- Firebase Cli
- Sendgrid account
- Google Maps and Geocoding API
---

## Setting it up:

### Basic:
- Docker https://docs.docker.com/install/
- Nodejs https://nodejs.org/en/download/

### Firebase:
- Create a free development account at https://firebase.google.com/
- Create a new project
- Enable Cloud Firestore [Beta] in "Test Mode"
- Go to "Authentication", and enable at least one authentication method (ie. Google)
- Go to "Storage", and click through the enabling process
- Go to "Settings" -> "General" and click "Add Firebase to your web app"
- Copy contents of `var config` to parameter labeled "firebase" in `public/src/app/environments/environment.ts`

### Firebase-cli:
- Install cli by following these instructions https://firebase.google.com/docs/cli/
- `cd firebase`
- `firebase login`
- `firebase use [PROJECT_ID]`    (check with 'firebase list')
- `cd firebase/functions && npm install`
- `firebase deploy --only functions`
- Go back to Firebase dashboard (set up above), click on "Functions"
- Copy validateServiceRequest endpoint link to parameter labeled "validationLink" in `services/mail.js`
- Copy deleteExpiredDocuments endpoint link to `cron/crontab_root` after `curl -s`

### Sendgrid:
- Create an account at https://app.sendgrid.com/signup
- Open your Sendgrid account
- Go to "Settings" -> "API Keys" --> "Create API Key" --> "Full Access"
- Copy the key, and save it to `credentials/sendgrid.json`

### Google Maps and Geocoding:
- Go to https://console.developers.google.com/projectselector/apis
- Select your (firebase) project
- Search for "Maps JavaScript API", select the API and click "ENABLE"
- Search for "Geocoding", select the API and click "ENABLE"
- Create an API key (Credentials-tab) if necessary, or copy the existing Browser Key to parameter labeled "googlemaps" in `public/src/app/environments/environment.ts`
---

## Running it:

### docker:

- `docker-compose up --build`

### localhost:

- `npm install`
- `cd public && npm install`
- `cd public && ng build --watch`
- `node server.js`

## Stuff to do:
- Writing Firebase rules (currently wide open)
- User database / authentication to allow for more features
- Probably a ton of other stuff I can't remember now
