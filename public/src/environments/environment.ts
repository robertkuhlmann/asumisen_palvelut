// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCWiiVaBx9Wwneh6PyRd0DE9lSL31Yf6R8",
    authDomain: "asumisen-palvelut.firebaseapp.com",
    databaseURL: "https://asumisen-palvelut.firebaseio.com",
    projectId: "asumisen-palvelut",
    storageBucket: "asumisen-palvelut.appspot.com",
    messagingSenderId: "709989013626"
  },
  serverAddress: 'http://a41d.k.time4vps.cloud:8080',
  googlemaps: "AIzaSyDoMKDRC6paSnZYAO8S7qRSboOeRVJJiIw"
};
