import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { ServicePageComponent } from './service-page.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NavbarComponent } from '../navbar/navbar.component';
import { PropertyInfoComponent } from '../service-page/components/property-info/property-info.component';
import { ServiceListComponent } from '../service-page/components/service-list/service-list.component';
import { InfoFeedComponent } from '../service-page/components/info-feed/info-feed.component';
import { MaintenanceFormComponent } from '../service-page/components/maintenance-form/maintenance-form.component';
import { SaunaFormComponent } from '../service-page/components/sauna-form/sauna-form.component';
import { LaundryFormComponent } from '../service-page/components/laundry-form/laundry-form.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';

import { environment } from '../../environments/environment';

describe('ServicePageComponent', () => {
  let component: ServicePageComponent;
  let fixture: ComponentFixture<ServicePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ServicePageComponent,
        NavbarComponent,
        PropertyInfoComponent,
        ServiceListComponent,
        MaintenanceFormComponent,
        InfoFeedComponent,
        SaunaFormComponent,
        LaundryFormComponent
      ],
      imports: [
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        MatProgressSpinnerModule,
        MatToolbarModule,
        MatIconModule,
        MatDividerModule,
        MatListModule,
        MatCardModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatSelectModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue:{params: Observable.of({property: 'testnavigation'})} }
      ]
    })
    .compileComponents();
  }));


  
  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePageComponent);
    component = fixture.componentInstance;
    component.property = {};
    component.propertyId = 'teststring';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
