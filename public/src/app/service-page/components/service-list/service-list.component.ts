import { Component, OnInit, Input } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.css']
})
export class ServiceListComponent implements OnInit {

  @Input('property') property;
  @Input('propertyId') propertyId;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  onClick(service){
    this.router.navigate([`/${this.propertyId}/${service}`]);
  }

}
