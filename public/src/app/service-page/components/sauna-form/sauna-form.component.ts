import { Component, OnInit, Input } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { AngularFirestore } from 'angularfire2/firestore';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/combineLatest';

import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-sauna-form',
  templateUrl: './sauna-form.component.html',
  styleUrls: ['./sauna-form.component.css']
})
export class SaunaFormComponent implements OnInit {

  @Input('propertyId') propertyId;
  @Input('property') property;
  submitted: Boolean = false;

  saunaForm: FormGroup;
  validDates: Array<number>;
  validHours: Array<any>;
  serviceRequests: Array<any>;
  dayFilter: any;
  minDate: Date;
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    private router: Router,
    public snackBar: MatSnackBar,
    private http: Http
  ) {}

  ngOnInit() {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate()+30);
    this.db.collection('properties')
      .doc(this.propertyId)
      .valueChanges()
      .subscribe(property => {
        this.property = property;
        this.validDates = this.property.saunaDays.map(day => {
          switch(day) {
            case 'sun':
              return 0;
            case 'mon':
              return 1;
            case 'tue':
              return 2;
            case 'wed':
              return 3;
            case 'thu':
              return 4;
            case 'fri':
              return 5;
            case 'sat':
              return 6;
          }
        });

    this.validHours = [];

    this.dayFilter = (d: Date): boolean => {
      const day = d.getDay();

      if(this.validDates.includes(day)) {
        return true;
      }
    }

    this.db.collection(
      'serviceRequest',
      ref => {
        return ref
          .where('propertyId', '==', this.propertyId)
          .where('type', '==', 'sauna');
      }
    ).valueChanges()
    .subscribe(values => {
        this.serviceRequests = values;
      });
    });

    this.saunaForm = this.formBuilder.group({
      tenantName: [null, [Validators.required, Validators.pattern(/^[A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]* [A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]*$/)]],
      tenantEmail: [null, [Validators.required, Validators.email]],
      tenantPhone: [null, [Validators.required, Validators.pattern(/^[\+0-9][0-9]*[ ]*[0-9]*$/)]],
      apartment: [null, [Validators.required, Validators.pattern(/^[A-Za-z0-9 ]+$/)]],
      saunaDay: [null, [Validators.required]],
      saunaStart: [{value: null, disabled: !this.property.saunaDay}, [Validators.required]]
    });

  }

  onDateChange(event) {

    const saunaStart = this.saunaForm.get('saunaStart');
    saunaStart.setValue(null);
    if(event.value) {
      const existingStartTimes: Array<any> = this.serviceRequests.map(request => {
        if(request.saunaDay.valueOf() === event.value.valueOf()) {
          return request.saunaStart;
        }
      });

      this.validHours = [];

      for(let i = this.property.saunaStart; i < this.property.saunaEnd; i++) {

        let state = existingStartTimes.includes(i) ? false : true;

        const hourSlot = {
          start: i,
          end: (i+1),
          free: state
        };
        this.validHours.push(hourSlot);
      }
      saunaStart.enable();
    } else {
      saunaStart.disable();
    }
  }

  async onSubmit() {
    this.submitted = true;
    let data = this.saunaForm.value;
    data.propertyId = this.propertyId;
    data.type = "sauna";
    try {
      let serviceRequest = await this.db.collection('serviceRequest').add(data);
      data.serviceRequestID = serviceRequest.id;
      await this.sendEmail(data);
      this.snackBar.open("Saunavaraus lähetetty!", "", {
        duration: 5000
      });
    } catch(err) {
      console.log(err);
      this.snackBar.open("Virhe saunavarauksen lähetyksessä.", "", {
        duration: 5000
      });
    }
    this.router.navigate([`/${this.propertyId}`]);
  }

  onCancel() {
    this.router.navigate([`/${this.propertyId}`]);
  }

  private sendEmail(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post('/api/saunaEmail', JSON.stringify(data), {headers})
      .subscribe(
        (res) => {
          resolve();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

}
