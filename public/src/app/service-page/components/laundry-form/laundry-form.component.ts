import { Component, OnInit, Input } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { AngularFirestore } from 'angularfire2/firestore';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/combineLatest';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-laundry-form',
  templateUrl: './laundry-form.component.html',
  styleUrls: ['./laundry-form.component.css']
})
export class LaundryFormComponent implements OnInit {

  @Input('propertyId') propertyId;
  @Input('property') property;
  submitted: Boolean = false;

  laundryForm: FormGroup;
  validDates: Array<number>;
  validHours: Array<any>;
  serviceRequests: Array<any>;
  dayFilter: any;
  minDate: Date;
  maxDate: Date;

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    private router: Router,
    public snackBar: MatSnackBar,
    private http: Http
  ) { }

  ngOnInit() {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.maxDate.setDate(this.maxDate.getDate()+30);

    this.db.collection('properties')
      .doc(this.propertyId)
      .valueChanges()
      .subscribe(property => {
        this.property = property;
        this.validDates = this.property.laundryDays.map(day => {
          switch(day) {
            case 'sun':
              return 0;
            case 'mon':
              return 1;
            case 'tue':
              return 2;
            case 'wed':
              return 3;
            case 'thu':
              return 4;
            case 'fri':
              return 5;
            case 'sat':
              return 6;
          }
        });

    this.validHours = [];

    this.dayFilter = (d: Date): boolean => {
      const day = d.getDay();

      if(this.validDates.includes(day)) {
        return true;
      }
    }

    this.db.collection(
      'serviceRequest',
      ref => {
        return ref
          .where('propertyId', '==', this.propertyId)
          .where('type', '==', 'laundry');
      }
    ).valueChanges()
    .subscribe(values => {
        this.serviceRequests = values;
      });
    });

    this.laundryForm = this.formBuilder.group({
      tenantName: [null, [Validators.required, Validators.pattern(/^[A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]* [A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]*$/)]],
      tenantEmail: [null, [Validators.required, Validators.email]],
      tenantPhone: [null, [Validators.required, Validators.pattern(/^[\+0-9][0-9]*[ ]*[0-9]*$/)]],
      apartment: [null, [Validators.required, Validators.pattern(/^[A-Za-z0-9 ]+$/)]],
      laundryDay: [null, [Validators.required]],
      laundryStart: [{value: null, disabled: !this.property.laundryDay}, [Validators.required]]
    });
  }

  onDateChange(event) {

    const laundryStart = this.laundryForm.get('laundryStart');
    laundryStart.setValue(null);
    if(event.value) {
      const existingStartTimes: Array<any> = this.serviceRequests.map(request => {
        if(request.laundryDay.valueOf() === event.value.valueOf()) {
          return request.laundryStart;
        }
      });

      this.validHours = [];

      let i = this.property.laundryStart;

      while(i < this.property.laundryEnd) {

        let state = existingStartTimes.includes(i) ? false : true;

        const hourSlot = {
          start: i,
          end: (i+2),
          free: state
        };
        this.validHours.push(hourSlot);
        i = i + 2;
      }
      laundryStart.enable();
    } else {
      laundryStart.disable();
    }
  }

  async onSubmit() {
    this.submitted = true;
    let data = this.laundryForm.value;
    data.propertyId = this.propertyId;
    data.type = "laundry";
    try {
      let serviceRequest = await this.db.collection('serviceRequest').add(data);
      data.serviceRequestID = serviceRequest.id;
      await this.sendEmail(data);
      this.snackBar.open("Pyykkitupavaraus lähetetty!", "", {
        duration: 5000
      });
    } catch(err) {
      console.log(err);
      this.snackBar.open("Virhe pyykkitupavarauksen lähetyksessä.", "", {
        duration: 5000
      });
    }
    this.router.navigate([`/${this.propertyId}`]);
  }

  onCancel() {
    this.router.navigate([`/${this.propertyId}`]);
  }

  private sendEmail(data) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post('/api/laundryEmail', JSON.stringify(data), {headers})
      .subscribe(
        (res) => {
          resolve();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

}
