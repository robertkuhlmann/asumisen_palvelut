import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

interface IPost {
  id: string;
  title: string;
  content: string;
  created: Date;
}

interface IInstruction {
  id: string;
  title: string;
  file: string;
  created: Date;
}

@Component({
  selector: 'app-info-feed',
  templateUrl: './info-feed.component.html',
  styleUrls: ['./info-feed.component.css']
})
export class InfoFeedComponent implements OnInit {

  @Input('propertyId') propertyId: string;
  private postRef: AngularFirestoreCollection<IPost>;
  private instructionRef: AngularFirestoreCollection<IInstruction>;

  private post$: Observable<any>;
  private instruction$: Observable<any>;

  private posts: Array<IPost>;
  private instructions: Array<IInstruction>;

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit() {

    this.postRef = this.db.collection(
      'posts',
      ref => ref.where(
        'propertyId',
        '==',
        this.propertyId
      )
    );

    this.instructionRef = this.db.collection(
      'instructions',
      ref => ref.where(
        'propertyId',
        '==',
        this.propertyId
      )
    );

    this.post$ = this.postRef
      .snapshotChanges()
      .map(changes => {
        return changes.map(item => {
          const data = item.payload.doc.data() as IPost;
          const id = item.payload.doc.id;

          /* Palvelimen aikaleiman parsiminen */
          const timestamp = data.created;
          const day = timestamp.getDate();
          const month = (timestamp.getMonth() + 1);
          const year = timestamp.getFullYear();
          const createdAt = `${day}.${month}.${year}`;

          return { id, ...data, createdAt };
        });
      });

      this.instruction$ = this.instructionRef
        .snapshotChanges()
        .map(changes => {
          return changes.map(item => {
            const data = item.payload.doc.data() as IInstruction;
            const id = item.payload.doc.id;

            const timestamp = data.created;
            const day = timestamp.getDate();
            const month = (timestamp.getMonth() + 1);
            const year = timestamp.getFullYear();
            const createdAt = `${day}.${month}.${year}`;

            return { id, ...data, createdAt };
          });
        });

    this.post$.subscribe(data => {
      this.posts = data;
      console.log(data);
    });

    this.instruction$.subscribe(data => {
      this.instructions = data;
      console.log(data);
    });
  }

  onInstructionClick(filePath) {
    window.open(filePath, '_blank');
  }

}
