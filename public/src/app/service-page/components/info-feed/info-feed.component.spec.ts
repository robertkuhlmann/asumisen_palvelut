import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFeedComponent } from './info-feed.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material';
import { MatListModule } from '@angular/material';

import { environment } from '../../../../environments/environment';

describe('InfoFeedComponent', () => {
  let component: InfoFeedComponent;
  let fixture: ComponentFixture<InfoFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InfoFeedComponent
      ],
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        MatCardModule,
        MatIconModule,
        MatListModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFeedComponent);
    component = fixture.componentInstance;
    component.propertyId = 'testString';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
