import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-property-info',
  templateUrl: './property-info.component.html',
  styleUrls: ['./property-info.component.css']
})
export class PropertyInfoComponent implements OnInit {

  @Input('property') property;
  @Input('propertyMaintenance') propertyMaintenance;

  constructor() {}

  ngOnInit() {
  }

}
