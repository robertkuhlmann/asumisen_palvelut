import { Component, OnInit, Input } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Http, Headers, Response } from '@angular/http';

import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-maintenance-form',
  templateUrl: './maintenance-form.component.html',
  styleUrls: ['./maintenance-form.component.css']
})
export class MaintenanceFormComponent implements OnInit {

  @Input('propertyId') propertyId;
  @Input('property') property;
  submitted: Boolean = false;
  file: File;

  serviceRequestForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    private router: Router,
    public snackBar: MatSnackBar,
    private http: Http,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.serviceRequestForm = this.formBuilder.group({
      tenantName: [null, [Validators.required, Validators.pattern(/^[A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]* [A-ZÄÖÅ][a-zA-ZÄÖäöÅå\-]*$/)]],
      tenantEmail: [null, [Validators.required, Validators.email]],
      tenantPhone: [null, [Validators.required, Validators.pattern(/^[\+0-9][0-9]*[ ]*[0-9]*$/)]],
      description: [null, [Validators.required]],
      apartment: [null, [Validators.required, Validators.pattern(/^[A-Za-z0-9 ]+$/)]]
    });
  }

  async onSubmit(){
    this.submitted = true;
    let data = this.serviceRequestForm.value;
    data.propertyId = this.propertyId;
    data.maintenanceId = this.property.propertyMaintenanceId;
    data.type = "maintenance";
    try{
      if(this.file){
        data.picture = await this.startUpload();
      }
      let servRequest = await this.db.collection('serviceRequest').add(data);
      data.serviceRequestID = servRequest.id;
      await this.sendEmail(data);
      this.snackBar.open("Palvelupyyntö lähetetty!", "", {
        duration: 5000
      });
    }
    catch(err){
      console.error(err);
      this.snackBar.open("Virhe palvelupyynnön lähetyksessä!", "", {
        duration: 5000
      });
    }
    this.router.navigate([`/${this.propertyId}`]);
  }

  onCancel(){
    this.router.navigate([`/${this.propertyId}`]);
  }

  // API call to send email.
  private sendEmail(data){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post('/api/serviceRequestEmail', JSON.stringify(data), {headers:headers})
      .subscribe(
        (res) => {
          resolve();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  // Read user file.
  getFile(event: FileList){
    let file = event.item(0);
    if(file.type.split('/')[0] != 'image'){
      this.file = undefined;
      this.snackBar.open("Virheellinen tiedostomuoto!", "", {
        duration: 5000
      });
      return;
    } else if(file.size > 2000000){
      this.file = undefined;
      this.snackBar.open("Liian suuri tiedoston koko!", "", {
        duration: 5000
      });
      return;
    }
    this.file = file;
  }

  // Upload file to FireStorage and return downloadUrl.
  startUpload() {
    return new Promise((resolve,reject) => {
      try{
        let file = this.file;
        
        const path = `serviceRequest/${new Date().getTime()}_${file.name}`;
    
        // The main task
        let task = this.storage.upload(path, file);
    
        // Progress monitoring
        task.snapshotChanges().subscribe((x) => {
          x.task.on('state_changed', (event) => {
          },
          (err) => {
            console.log(err);
            throw err;
          },
          () => {
            task.downloadURL().subscribe((value) => {
              resolve(value);
            });
          });
        }); 
      }
      catch(err){
        reject(err);
      }
    });
  }
}
