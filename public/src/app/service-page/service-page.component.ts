import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.css']
})
export class ServicePageComponent implements OnInit {

  propertyId: string = null;
  property = null;
  propertyMaintenance = null;
  service = null;

  constructor(
    private route: ActivatedRoute,
    private db: AngularFirestore
  ) {
    this.route.params.subscribe(params => {

      //FIRESTORE PROPERTY / MAINTENANCE INFO
      this.db.collection('properties')
        .doc(params['property'])
        .valueChanges()
        .subscribe(value => {
          this.property = value;
          this.propertyId = params['property'];
          //console.log(this.property);
          this.db.collection('property-maintenance')
            .doc(this.property.propertyMaintenanceId)
            .valueChanges()
            .subscribe(maintenance => {
              this.propertyMaintenance = maintenance;
            });
        });
        
        this.service = params['service'];

    });
  }

  ngOnInit() {
  }

}
