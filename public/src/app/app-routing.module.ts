import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { AdminComponent } from './admin/admin.component';
import { MaintenanceFormComponent } from './service-page/components/maintenance-form/maintenance-form.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'admin/settings',
    component: AdminComponent
  },
  {
    path: 'admin/properties',
    component: AdminComponent
  },
  {
    path: 'admin/info-feed',
    component: AdminComponent
  },
  {
    path: 'admin/properties/new',
    component: AdminComponent
  },
  {
    path: 'admin/services',
    component: AdminComponent
  },
  {
    path: 'admin/users',
    component: AdminComponent
  },
  {
    path: 'admin/servicerequests',
    component: AdminComponent
  },
  {
    path: ':property',
    component: ServicePageComponent
  },
  {
    path: ':property/:service',
    component: ServicePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
