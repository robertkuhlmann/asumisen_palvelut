import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AdminComponent } from './admin.component';
import { MatIcon } from '@angular/material/icon';
import { MatToolbarModule, MatSidenavModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { AdminSettingsComponent } from './components/admin-settings/admin-settings.component';
import { AdminPropertiesComponent } from './components/admin-properties/admin-properties.component';
import { AdminInfoFeedComponent } from './components/admin-info-feed/admin-info-feed.component';
import { AdminServicesComponent } from './components/admin-services/admin-services.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { AdminServiceRequestsComponent } from './components/admin-service-requests/admin-service-requests.component';
import { AdminInitComponent } from './components/admin-init/admin-init.component';
import { AdminMenuComponent } from './components/admin-menu/admin-menu.component';
import { PropertyNewComponent } from './components/admin-properties/property-new/property-new.component';
import { QRCodeModule } from 'angular2-qrcode';
import { AgmCoreModule } from '@agm/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { environment } from '../../environments/environment';
import { MediaMatcher } from '@angular/cdk/layout';

//MatToolbarRow, MatToolbar, MatSidenav, MatNavList, MatSpinner, MatSidenavContent, 
describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminComponent,
        AdminSettingsComponent,
        AdminPropertiesComponent,
        AdminInfoFeedComponent,
        AdminServicesComponent,
        AdminUsersComponent,
        AdminMenuComponent,
        AdminServiceRequestsComponent,
        AdminInitComponent,
        PropertyNewComponent
      ],
      imports: [
        RouterTestingModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        MatSlideToggleModule,
        FormsModule,
        ReactiveFormsModule,
        QRCodeModule,
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AgmCoreModule.forRoot({
          apiKey: environment.googlemaps
        })
      ],
      providers: [
        MediaMatcher
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
