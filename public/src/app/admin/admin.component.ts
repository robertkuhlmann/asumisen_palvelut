import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { AuthService } from '../providers/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { MediaMatcher } from '@angular/cdk/layout';
import * as firebase from 'firebase/app';
//import { MatIconModule } from '@angular/material/icon';

/* Kiinteistöhuollon interface */
interface IPropertyMaintenance {
  id?: string;
  maintenanceName: string;
  maintenanceEmail: string;
  maintenanceNumber: string;
}

/* Kiinteistön interface */
interface IProperty {
  id?: string;
  address: string;
  city: string;
  postalCode: string;
  sauna?: boolean; // ?-merkillä merkitään ei-pakolliset arvot
  maintenance?: boolean;
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  // Menun responsivisuuden hallinta
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  // Reitin parametri, jolla navigoidaan käyttöliittymässä
  private routeValue: string = null;

  // Tietokannasta haettu käyttäjä
  private user: any;
  private userID: string;

  // Kiinteistöhuollon id
  private propertyMaintenanceId: string;

  // Tietokantaviitteet: kiinteistöhuolto, kiinteistöt
  private propertyMaintenanceRef: AngularFirestoreCollection<IPropertyMaintenance>;
  private propertyRef: AngularFirestoreCollection<IPropertyMaintenance>;

  // Observablet: kiinteistöhuolto, kiinteistöt
  private propertyMaintenance$: Observable<any>;
  private property$: Observable<any>;

  // Kiinteistöhuollon ja kiinteistöjen data, lähetetään lapsikomponenteille
  public propertyMaintenance: IPropertyMaintenance;
  public properties: Array<IProperty>;

  // Kirjautumisen tila
  loginState: any;

  private loading: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private auth: AngularFireAuth,
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private db: AngularFirestore
  ) {

    this.loading = true;

    /* Kirjautumisen tilan tarkistus, ei-kirjautuneena arvo null */
    this.auth.authState.subscribe(state => {
      if(state) {

        this.userID = state.uid;

        /* Haetaan users-taulusta kirjautuneen käyttäjän kiinteistöhuollon id */
        this.db.collection('users')
          .doc(state.uid)
          .valueChanges()
          .subscribe(data => {

            if(!data) {

              this.user = null;
              this.propertyMaintenanceId = null;
              this.propertyMaintenance = null;

              this.loading = false;

            } else {
              /* Tallennetaan käyttäjän data */
              this.user = data;

              /* Asetetaan kiinteistöhuollon id muuttujaan */
              this.propertyMaintenanceId = this.user.maintenanceId;

              /* Alustetaan tietokantaviite kiinteistöhuoltoon */
              this.propertyMaintenanceRef = this.db.collection('property-maintenance');

              /* Luodaan kiinteistöhuollon Observable */
              this.propertyMaintenance$ = this.propertyMaintenanceRef
                .doc(this.propertyMaintenanceId)
                .snapshotChanges()
                .map(item => {
                  const data = item.payload.data() as IPropertyMaintenance;
                  const id = item.payload.id; // Tallennetaan myös id
                  return { id, ...data }; // Observable palauttaa uuden olion
                });

              /* Alustetaan tietokantaviite kiinteistöhin */
              this.propertyRef = this.db.collection(
                'properties',
                /* Query jolla haetaan kiinteistöt, jotka ovat liitetty kiinteistöhuoltoon */
                ref => ref.where(
                  'propertyMaintenanceId',
                  '==',
                  this.propertyMaintenanceId
                )
              );

              /* Luodaan kiinteistöjen Observable */
              this.property$ = this.propertyRef
                .snapshotChanges()
                .map(changes => {
                  return changes.map(item => {
                    const data = item.payload.doc.data() as IProperty;
                    const id = item.payload.doc.id;
                    return { id, ...data };
                  });
                });

              /* Subscribe luotuun kiinteistöhuollon Observableen */
              this.propertyMaintenance$.subscribe(data => {
                this.propertyMaintenance = data;
              });

              /* Subscribe luotuun kiinteistöjen Observableen */
              this.property$.subscribe(data => {
                this.properties = data;
              })

              /* Tarkastetaan route, renderöidään oikeat komponentit sen perusteella */
              this.route.url.subscribe(url => {
                if(url.length == 2) {
                  this.routeValue = url[1].path;
                }
              });

              this.loading = false;
            }
          });

      /* Responsiivisen menun asetukset */
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }
    });
  }

  ngOnInit() { }

  /* Sisäänkirjautuminen */
  login() {
    this.auth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  /* Uloskirjautuminen */
  logout() {
    this.auth.auth.signOut();
  }

  /* Oikeaan reittiin navigointi */
  navigateTo(adminRoute) {
    if(!adminRoute) {
      this.router.navigate(['/admin']);
    } else {
      this.router.navigate([`/admin/${adminRoute}`]);
    }
  }

  /* Komponentin tuhoutuessa poistetaan responsiivisuutta hallitseva listener */
  ngOnDestroy(): void {
    if(this.mobileQuery){
      this.mobileQuery.removeListener(this._mobileQueryListener);
    }
  }

}
