import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminServiceRequestsComponent } from './admin-service-requests.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AgmCoreModule } from '@agm/core';

import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

import { environment } from '../../../../environments/environment';

describe('AdminServiceRequestsComponent', () => {
  let component: AdminServiceRequestsComponent;
  let fixture: ComponentFixture<AdminServiceRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminServiceRequestsComponent
      ],
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        MatCardModule,
        MatDividerModule,
        MatDialogModule,
        MatSelectModule,
        MatSnackBarModule,
        FormsModule,
        AgmCoreModule.forRoot({
          apiKey: environment.googlemaps
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminServiceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
