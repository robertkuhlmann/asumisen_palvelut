import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { MatDialog, MatDialogRef, MatSnackBar, MatSelect } from '@angular/material';

import { MapsAPILoader } from '@agm/core';
declare var google: any;

@Component({
  selector: 'app-admin-service-requests',
  templateUrl: './admin-service-requests.component.html',
  styleUrls: ['./admin-service-requests.component.css']
})
export class AdminServiceRequestsComponent implements OnInit {

  constructor(
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    private snackBar: MatSnackBar,
    public mapsApiLoader: MapsAPILoader
  ) { }

  serviceRequests = null;
  maintenanceId = null;
  statusList = ["Vastaanotettu","Käsittelyssä","Huollettu"];

  // google maps
  propertyLatitude: String;
  propertyLongitude: String;
  geocoder;
  mapOk: Boolean = false;

  async updateRequestStatus(request){
    let data = {
      status: request.status
    }
    await this.db.collection('serviceRequest').doc(request.id).update(data);
    this.snackBar.open("Tila päivitetty.", "", {
      duration: 2000
    });
  }

  ngOnInit() {
    this.mapsApiLoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });

    this.auth.authState.subscribe(state => {
      if(state){
        // haetaan authorisoitunut käyttäjä
        this.db.collection('users').doc(state.uid).valueChanges().subscribe((values) => {
          this.maintenanceId = values["maintenanceId"];

          // haetaan taloyhtiölle tulleet palvelupyynnöt
          this.db.collection(
            'serviceRequest',
            ref => {
              return ref
              .where('maintenanceId', '==', this.maintenanceId)
              .where('type', '==', 'maintenance');
            }
          ).snapshotChanges()
          .subscribe(changes => {
            let tmpList = [];
            for(let change of changes){
              let item = {
                ...change.payload.doc.data(),
                id: change.payload.doc.id
              }
              tmpList.push(item);
            }
            this.serviceRequests = tmpList;


            // haetaan kiinteistön tiedot.
            for(let request of this.serviceRequests){

              // alustetaan status jos se on väärä.
              if(!this.statusList.includes(request.status)){
                request.status = this.statusList[0];
              }
              this.db.collection('properties').doc(request.propertyId).valueChanges().subscribe((property) => {
                this.geocoder.geocode({"address":property['address']+" "+property['city']}, (results, status) => {
                  if(status == "OK"){
                    property['latitude'] = results[0].geometry.location.lat();
                    property['longitude'] = results[0].geometry.location.lng();
                    this.mapOk = true;
                  }
                  request.propertyInfo = property;
                });
              });
            }
          });
        });
      }
    });
  }

}
