import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPropertiesComponent } from './admin-properties.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QRCodeModule } from 'angular2-qrcode';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AgmCoreModule } from '@agm/core';

import { environment } from '../../../../environments/environment';

import { RemovePropertyComponent } from './remove-property/remove-property.component';
import { UpdatePropertyComponent } from './update-property/update-property.component';
import { PropertyNewComponent } from './property-new/property-new.component';

describe('AdminPropertiesComponent', () => {
  let component: AdminPropertiesComponent;
  let fixture: ComponentFixture<AdminPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AdminPropertiesComponent,
        RemovePropertyComponent,
        UpdatePropertyComponent,
        PropertyNewComponent
      ],
      imports: [
        MatCardModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatSnackBarModule,
        FormsModule,
        ReactiveFormsModule,
        QRCodeModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AgmCoreModule.forRoot({
          apiKey: environment.googlemaps
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
