import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { MapsAPILoader } from '@agm/core';
import {Observable}  from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

declare var google: any;

@Component({
  selector: 'app-update-property',
  templateUrl: './update-property.component.html',
  styleUrls: ['./update-property.component.css']
})
export class UpdatePropertyComponent implements OnInit {

  updatePropertyForm: FormGroup;
  timeOptions: Array<number> = [9,10,11,12,13,14,15,16,17,18,19,20,21,22];
  timeOptionsPaired: Array<number> = [8, 10, 12, 14, 16, 18, 20];
  dayOptions: Array<any> = [
    { text: 'Maanantai', value: 'mon' },
    { text: 'Tiistai', value: 'tue' },
    { text: 'Keskiviikko', value: 'wed' },
    { text: 'Torstai', value: 'thu' },
    { text: 'Perjantai', value: 'fri' },
    { text: 'Lauantai', value: 'sat' },
    { text: 'Sunnuntai', value: 'sun' },
  ];

  // google maps
  defaultZoom: number = 14;
  propertyLatitude: number;
  propertyLongitude: number;
  geocoder;
  mapOk = false;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdatePropertyComponent>,
    @Inject(MAT_DIALOG_DATA) private property,
    public mapsApiLoader: MapsAPILoader
  ) { }

  updateMapPosition(address){
    this.geocoder.geocode({"address":address}, (results, status) => {
      if(status == "OK"){
        this.propertyLatitude = results[0].geometry.location.lat();
        this.propertyLongitude = results[0].geometry.location.lng();
        this.mapOk = true;
      } else{
        this.mapOk = false;
      }
    });
  }

  ngOnInit() {

    this.mapsApiLoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
      this.updateMapPosition(this.property.address+" "+this.property.city);
    });

    this.updatePropertyForm = this.formBuilder.group({
      address: [this.property.address, [Validators.required]],
      city: [this.property.city, [Validators.required]],
      postalCode: [this.property.postalCode, [Validators.required, Validators.maxLength(5)]],
      sauna: [this.property.sauna],
      saunaStart: [{
        value: this.property.saunaStart,
        disabled: !this.property.sauna}],
      saunaEnd: [{
        value: this.property.saunaEnd,
        disabled: !this.property.sauna}],
      saunaDays: [{
        value: this.property.saunaDays,
        disabled: !this.property.sauna}],
      laundry: [this.property.laundry],
      laundryStart: [{
        value: this.property.laundryStart,
        disabled: !this.property.laundry}],
      laundryEnd: [{
        value: this.property.laundryEnd,
        disabled: !this.property.laundry}],
      laundryDays: [{
        value: this.property.laundryDays,
        disabled: !this.property.laundry}],
      maintenance: [this.property.maintenance]
    });

    this.updatePropertyForm.valueChanges.debounceTime(1000).subscribe(val => {
      this.updateMapPosition(val.address+" "+val.city);
    });
  }

  handleSaunaChange(value) {
    const saunaStart = this.updatePropertyForm.get('saunaStart');
    const saunaEnd = this.updatePropertyForm.get('saunaEnd');
    const saunaDays = this.updatePropertyForm.get('saunaDays');

    if(value.checked == true) {
      saunaStart.enable();
      saunaEnd.enable();
      saunaDays.enable();
    } else if(value.checked == false) {
      saunaStart.disable();
      saunaEnd.disable();
      saunaDays.disable();
    }
  }

  handleLaundryChange(value) {
    const laundryStart = this.updatePropertyForm.get('laundryStart');
    const laundryEnd = this.updatePropertyForm.get('laundryEnd');
    const laundryDays = this.updatePropertyForm.get('laundryDays');

    if(value.checked == true) {
      laundryStart.enable();
      laundryEnd.enable();
      laundryDays.enable();
    } else if(value.checked == false) {
      laundryStart.disable();
      laundryEnd.disable();
      laundryDays.disable();
    }
  }

  confirmUpdate(form) {
    this.dialogRef.close(form.value);
  }

}
