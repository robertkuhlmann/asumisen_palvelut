import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemovePropertyComponent } from './remove-property.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('RemovePropertyComponent', () => {
  let component: RemovePropertyComponent;
  let fixture: ComponentFixture<RemovePropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RemovePropertyComponent
      ],
      imports: [
        MatDialogModule
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemovePropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
