import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-remove-property',
  templateUrl: './remove-property.component.html',
  styleUrls: ['./remove-property.component.css']
})
export class RemovePropertyComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<RemovePropertyComponent>,
    @Inject(MAT_DIALOG_DATA) private property
  ) { }

  ngOnInit() {
  }

  confirmRemove() {
    this.dialogRef.close(true);
  }

  cancelRemove() {
    this.dialogRef.close(false);
  }

}
