import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { RemovePropertyComponent } from './remove-property/remove-property.component';
import { UpdatePropertyComponent } from './update-property/update-property.component';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-admin-properties',
  templateUrl: './admin-properties.component.html',
  styleUrls: ['./admin-properties.component.css']
})
export class AdminPropertiesComponent implements OnInit {

  // Isäntäkomponentista haetut kiinteistöt ja kiinteistöhuollon id
  @Input('properties') properties;
  @Input('propertyMaintenance') propertyMaintenance;

  // Kiinteistön lisäämisformin näkyvyys
  newPropertyFormVisible: Boolean = false;
  removePropertyDialogRef: MatDialogRef<RemovePropertyComponent>;
  updatePropertyDialogRef: MatDialogRef<UpdatePropertyComponent>;
  serverAddress = environment.serverAddress;

  constructor(
    private dialog: MatDialog,
    private db: AngularFirestore,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {}

  // Kiinteistön lisäämisformin näkyvyyden hallinta
  toggleNewPropertyForm(value) {
    this.newPropertyFormVisible = value;
  }

  openRemoveDialog(property) {
    this.removePropertyDialogRef = this.dialog.open(RemovePropertyComponent, {
      data: {
        address: property.address
      }
    });

    this.removePropertyDialogRef
      .afterClosed()
      .subscribe(shouldRemove => {

        if(shouldRemove) {
          this.db.collection('properties')
            .doc(property.id)
            .delete()
            .then(() => {
              this.snackBar.open("Kiinteistö poistettu.", "", {
                duration: 5000
              });
            }, (err) => {
              console.log(err);
              this.snackBar.open("Kiinteistön poistaminen epäonnstui.", "", {
                duration: 5000
              });
            });
        }
      });
  }

  openUpdateDialog(property) {
    this.updatePropertyDialogRef = this.dialog.open(UpdatePropertyComponent, {
      data: property
    });

    this.updatePropertyDialogRef
      .afterClosed()
      .subscribe(formData => {
        if(formData) {
          this.db.collection('properties')
            .doc(property.id)
            .update(formData)
            .then(() => {
              this.snackBar.open("Kiinteistön tiedot päivitetty.", "", {
                duration: 5000
              });
            }, (err) => {
              console.log(err);
              this.snackBar.open(
                "Kiinteistön tietojen päivittäminen epäonnistui",
                "",
                { duration: 5000}
              );
            });
        }
      });
  }

}
