import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { MatSnackBar } from '@angular/material';

import { MapsAPILoader } from '@agm/core';
import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

declare var google: any;

@Component({
  selector: 'app-property-new',
  templateUrl: './property-new.component.html',
  styleUrls: ['./property-new.component.css']
})
export class PropertyNewComponent implements OnInit {

  @Input('propertyMaintenance') propertyMaintenance;

  newPropertyForm: FormGroup;
  timeOptions: Array<number> = [9,10,11,12,13,14,15,16,17,18,19,20,21,22];
  timeOptionsPaired: Array<number> = [8, 10, 12, 14, 16, 18, 20];
  dayOptions: Array<any> = [
    { text: 'Maanantai', value: 'mon' },
    { text: 'Tiistai', value: 'tue' },
    { text: 'Keskiviikko', value: 'wed' },
    { text: 'Torstai', value: 'thu' },
    { text: 'Perjantai', value: 'fri' },
    { text: 'Lauantai', value: 'sat' },
    { text: 'Sunnuntai', value: 'sun' },
  ];

  // google maps
  defaultZoom: number = 14;
  propertyLatitude: number;
  propertyLongitude: number;
  geocoder;
  mapOk = false;
  updateMapPosition(address){
    this.geocoder.geocode({"address":address}, (results, status) => {
      if(status == "OK"){
        this.propertyLatitude = results[0].geometry.location.lat();
        this.propertyLongitude = results[0].geometry.location.lng();
        this.mapOk = true;
      } else{
        this.mapOk = false;
      }
    });
  }

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    private snackBar: MatSnackBar,
    public mapsApiLoader: MapsAPILoader
  ) { }


  ngOnInit() {

    this.mapsApiLoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });

    this.newPropertyForm = this.formBuilder.group({
      address: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required, Validators.maxLength(5)]],
      sauna: [false],
      saunaStart: [{value: this.timeOptions[0], disabled: true}],
      saunaEnd: [{value: this.timeOptions[0], disabled: true}],
      saunaDays: [{value: null, disabled: true}],
      laundry: [false],
      laundryStart: [{value: this.timeOptionsPaired[0], disabled: true}],
      laundryEnd: [{value: this.timeOptionsPaired[0], disabled: true}],
      laundryDays: [{value: null, disabled: true}],
      maintenance: [false]
    });
    
    this.newPropertyForm.valueChanges.debounceTime(1000).subscribe(val => {
      this.updateMapPosition(val.address+" "+val.city);
    });

  }

  handleSaunaChange(value) {
    const saunaStart = this.newPropertyForm.get('saunaStart');
    const saunaEnd = this.newPropertyForm.get('saunaEnd');
    const saunaDays = this.newPropertyForm.get('saunaDays');

    if(value.checked == true) {
      saunaStart.enable();
      saunaEnd.enable();
      saunaDays.enable();
    } else if(value.checked == false) {
      saunaStart.disable();
      saunaEnd.disable();
      saunaDays.disable();
    }
  }

  handleLaundryChange(value) {
    const laundryStart = this.newPropertyForm.get('laundryStart');
    const laundryEnd = this.newPropertyForm.get('laundryEnd');
    const laundryDays = this.newPropertyForm.get('laundryDays');

    if(value.checked == true) {
      laundryStart.enable();
      laundryEnd.enable();
      laundryDays.enable();
    } else if(value.checked == false) {
      laundryStart.disable();
      laundryEnd.disable();
      laundryDays.disable();
    }
  }

  async onSubmit() {
    let data = this.newPropertyForm.value;
    console.log(data);
    data.propertyMaintenanceId = this.propertyMaintenance.id;

    try {
      await this.db.collection('properties').add(data);
      this.snackBar.open("Uusi kiinteistö lisätty!", "", {
        duration: 5000
      });
    }
    catch(err) {
      console.log(err.message);
      this.snackBar.open("Kiinteistön lisääminen epäonnistui.", "", {
        duration: 5000
      });
    }
  }

}
