import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})
export class AdminSettingsComponent implements OnInit {

  @Input('propertyMaintenance') propertyMaintenance;

  propertyMaintenanceForm: FormGroup;
  submitted: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.propertyMaintenanceForm = this.formBuilder.group({
      maintenanceName: [
        this.propertyMaintenance.maintenanceName,
        [Validators.required]
      ],
      maintenanceEmail: [
        this.propertyMaintenance.maintenanceEmail,
        [Validators.required, Validators.email]
      ],
      maintenanceNumber: [
        this.propertyMaintenance.maintenanceNumber,
        [Validators.required, Validators.pattern(/^[\+0-9][0-9]*[ ]*[0-9]*$/)]
      ]
    });
  }

  async onSubmit() {
    this.submitted = true;
    let data = this.propertyMaintenanceForm.value;

    try {
      await this.db.collection('property-maintenance')
        .doc(this.propertyMaintenance.id)
        .update(data);

      this.snackBar.open("Tiedot päivitetty onnistuneesti.", "", {
        duration: 5000
      });

      this.router.navigate(['/admin']);

    } catch(err) {
      this.snackBar.open("Tietojen päivitys epäonnistui.", "", {
        duration: 5000
      });
    }
  }

  onCancel() {
    this.router.navigate(['/admin']);
  }

}
