import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.css']
})
export class PostNewComponent implements OnInit {

  newPostForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PostNewComponent>
  ) { }

  ngOnInit() {
    this.newPostForm = this.formBuilder.group({
      title: [null, [Validators.required]],
      content: [null, [Validators.required]]
    });
  }

  /*ngOnDestroy() {
    this.dialogRef.close(false);
  }*/

  addNewPost(form) {
    if(form.valid) {
      this.dialogRef.close(form.value);
    }
  }

  closeForm() {
    this.dialogRef.close(false);
  }

}
