import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { Http, Headers, Response } from '@angular/http';
import { PostNewComponent } from './post-new/post-new.component';
import { PostRemoveComponent } from './post-remove/post-remove.component';
import { PostUpdateComponent } from './post-update/post-update.component';
import { InstructionNewComponent } from './instruction-new/instruction-new.component';
import { InstructionRemoveComponent } from './instruction-remove/instruction-remove.component';
import * as firebase from 'firebase';

interface IPost {
  id: string;
  title: string;
  content: string;
  propertyId: string;
  created: Date;
}

interface IInstruction {
  id: string;
  title: string;
  created: Date;
}

@Component({
  selector: 'app-admin-info-feed',
  templateUrl: './admin-info-feed.component.html',
  styleUrls: ['./admin-info-feed.component.css']
})
export class AdminInfoFeedComponent implements OnInit {

  @Input('properties') properties: Array<any>;
  private selectedProperty: any;
  private posts: Array<any>;
  private postsRef: AngularFirestoreCollection<IPost>;
  private posts$: Observable<any>;
  private instructions: Array<any>;
  private instructionsRef: AngularFirestoreCollection<IInstruction>;
  private instructions$: Observable<any>;
  private loading: Boolean = false;
  newPostDialogRef: MatDialogRef<PostNewComponent>;
  removePostDialogRef: MatDialogRef<PostRemoveComponent>;
  updatePostDialogRef: MatDialogRef<PostUpdateComponent>;
  newInstructionDialogRef: MatDialogRef<InstructionNewComponent>;
  removeInstructionDialogRef: MatDialogRef<InstructionRemoveComponent>;

  constructor(
    private db: AngularFirestore,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private http: Http,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {

  }

  selectProperty(event) {
    const property = event.value;
    this.loading = true;
    this.selectedProperty = property;
    this.posts$ = this.db.collection(
      'posts',
      ref => ref.where(
        'propertyId',
        '==',
        property.id
      )
    ).snapshotChanges()
    .map(changes => {
      return changes.map(item => {
        const data = item.payload.doc.data() as IPost;
        const id = item.payload.doc.id;

        if(data.created) {
          const timestamp = data.created;
          const day = timestamp.getDate();
          const month = timestamp.getMonth() +1; // month is between 0 - 11
          const year = timestamp.getFullYear();
          const createdAt = `${day}.${month}.${year}`;

          console.log(timestamp);

          return { id, ...data, createdAt };
        } else {
          return { id, ...data };
        }
        
      });
    });

    this.instructions$ = this.db.collection(
      'instructions',
      ref => ref.where(
        'propertyId',
        '==',
        property.id
      )
    ).snapshotChanges()
    .map(changes => {
      return changes.map(item => {
        const data = item.payload.doc.data() as IInstruction;
        const id = item.payload.doc.id;


        if(data.created) {
          const timestamp = data.created;
          const day = timestamp.getDate();
          const month = timestamp.getMonth();
          const year = timestamp.getFullYear();
          const createdAt = `${day}.${month}.${year}`;

          return { id, ...data, createdAt };
        } else {
          return { id, ...data};
        } 
      });
    });

    this.posts$.subscribe(data => {
      this.posts = data;
      this.loading = false;
    });

    this.instructions$.subscribe(data => {
      this.instructions = data;
    })
  }

  openNewPostForm(propertyId) {
    this.newPostDialogRef = this.dialog.open(PostNewComponent);

    this.newPostDialogRef
      .backdropClick()
      .subscribe(event => {
        return;
      })

    this.newPostDialogRef
      .afterClosed()
      .subscribe(async formData => {
        if(formData && typeof formData !== 'undefined') {
          try {
            const post = {
              created: firebase.firestore.FieldValue.serverTimestamp(),
              propertyId,
              ...formData };
            await this.db.collection('posts').add(post);
            this.snackBar.open("Uusi ilmoitus julkaistu.", "", {
              duration: 5000
            });
          } catch(e) {
            console.log(e.message);
            this.snackBar.open("Ilmoitusta ei julkaistu.", "", {
              duration: 5000
            });
          }
        }
      });
  }

  openUpdatePostForm(post) {
    this.updatePostDialogRef = this.dialog.open(PostUpdateComponent, {
      data: { post }
    });

    this.updatePostDialogRef 
      .afterClosed()
      .subscribe(async formData => {
        try {
          if(formData && typeof formData !== 'undefined') {
            await this.db.collection('posts').doc(post.id).update(formData);
            this.snackBar.open("Ilmoitus päivitetty.", "", {
              duration: 5000
            });
          }
        } catch(e) {
          console.log(e.message);
          this.snackBar.open("Virhe ilmoituksen päivityksessä.", "", {
            duration: 5000
          });
        }
      })
  }

  openRemovePostForm(post) {
    this.removePostDialogRef = this.dialog.open(PostRemoveComponent, {
      data: { postTitle: post.title }
    });

    this.removePostDialogRef
      .afterClosed()
      .subscribe(async data => {
        if(data !== false) {
          try {
            await this.db.collection('posts').doc(post.id).delete();
            this.snackBar.open("Ilmoitus poistettu.", "", {
              duration: 5000
            });
          } catch(e) {
            console.log(e.message);
            this.snackBar.open("Virhe ilmoituksen poistossa.", "", {
              duration: 5000
            });
          }
        }
      });
  }

  openNewInstructionForm(propertyId) {
    this.newInstructionDialogRef = this.dialog.open(InstructionNewComponent);

    this.newInstructionDialogRef
      .afterClosed()
      .subscribe(async formData => {
        if(
          formData.formValue &&
          typeof formData.formValue !== 'undefined' &&
          formData.file !== undefined
        ) {
          try {
            const filePath = await this.startUpload(formData.file);
            const instruction = {
              created: firebase.firestore.FieldValue.serverTimestamp(),
              propertyId,
              title: formData.formValue.title,
              file: filePath
            };
            await this.db.collection('instructions').add(instruction);
            this.snackBar.open("Uusi ohje lisätty", "", {
              duration: 5000
            });
          } catch(e) {
            console.log(e);
            this.snackBar.open("Ohjeen lisäämissä tapahtui ongelma.", "", {
              duration: 5000
            });
          }
        }
      })
  }

  startUpload(file: File) {
    return new Promise((resolve, reject) => {
      try {
        const path = `instruction/${new Date().getTime()}_${file.name}`;
        let task = this.storage.upload(path, file);

        task.snapshotChanges().subscribe(x => {
          x.task.on('state_changed', event => {
          },
          err => {
            console.log(err);
            throw err;
          },
          () => {
            task.downloadURL().subscribe(value => {
              resolve(value);
            });
          });
        });
      } catch(err) {
        reject(err);
      }
    });
  }

  openRemoveInstructionForm(instruction) {
    this.removeInstructionDialogRef = this.dialog.open(InstructionRemoveComponent, {
      data: { instructionTitle: instruction.title }
    });

    this.removeInstructionDialogRef
      .afterClosed()
      .subscribe(async data => {
        if(data !== false) {
          try {
            await this.db.collection('instructions').doc(instruction.id).delete();
            this.snackBar.open("Ohje poistettu.", "", {
              duration: 5000
            });
          } catch(e) {
            console.log(e);
            this.snackBar.open("Ohje poistettu.", "", {
              duration: 5000
            });
          }
        }
      });
  }

}
