import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Http, Headers, Response } from '@angular/http';

import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-instruction-new',
  templateUrl: './instruction-new.component.html',
  styleUrls: ['./instruction-new.component.css']
})
export class InstructionNewComponent implements OnInit {

  newInstructionForm: FormGroup;
  file: File;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<InstructionNewComponent>,
    private http: Http,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.newInstructionForm = this.formBuilder.group({
      title: [null, [Validators.required]]
    });
  }

  addNewInstruction(form) {
    if(form.valid && this.file !== undefined) {
      this.dialogRef.close({
        formValue: form.value,
        file: this.file
      });
    }
  }

  getFile(event: FileList) {
    let file = event.item(0);
    if(file.size > 2000000) {
      this.file = undefined;
      this.snackBar.open("Liian suuri tiedoston koko!", "", {
        duration: 5000
      });
      return;
    }
    this.file = file;
  }

  closeForm() {
    this.dialogRef.close(false);
  }

}
