import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-instruction-remove',
  templateUrl: './instruction-remove.component.html',
  styleUrls: ['./instruction-remove.component.css']
})
export class InstructionRemoveComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<InstructionRemoveComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
  }

  confirmDelete() {
    this.dialogRef.close(true);
  }

  cancelDelete() {
    this.dialogRef.close(false);
  }

}
