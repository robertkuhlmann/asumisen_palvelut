import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-post-remove',
  templateUrl: './post-remove.component.html',
  styleUrls: ['./post-remove.component.css']
})
export class PostRemoveComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<PostRemoveComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
  }

  confirmDelete() {
    this.dialogRef.close(true);
  }

  cancelDelete() {
    this.dialogRef.close(false);
  }

}
