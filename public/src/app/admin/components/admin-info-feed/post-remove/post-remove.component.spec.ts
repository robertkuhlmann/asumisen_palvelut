import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostRemoveComponent } from './post-remove.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('PostRemoveComponent', () => {
  let component: PostRemoveComponent;
  let fixture: ComponentFixture<PostRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostRemoveComponent
      ],
      imports: [
        MatDialogModule
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
