import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-post-update',
  templateUrl: './post-update.component.html',
  styleUrls: ['./post-update.component.css']
})
export class PostUpdateComponent implements OnInit {

  updatePostForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<PostUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit() {
    this.updatePostForm = this.formBuilder.group({
      title: [this.data.post && this.data.post.title, [Validators.required]],
      content: [this.data.post && this.data.post.content, [Validators.required]]
    });
  }

  updatePost(form) {
    this.dialogRef.close(form.value);
  }

  cancelUpdate() {
    this.dialogRef.close(false);
  }

}
