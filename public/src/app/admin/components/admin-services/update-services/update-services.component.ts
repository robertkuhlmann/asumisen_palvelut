import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-services',
  templateUrl: './update-services.component.html',
  styleUrls: ['./update-services.component.css']
})
export class UpdateServicesComponent implements OnInit {

  updateServicesForm: FormGroup;
  timeOptions: Array<number> = [9,10,11,12,13,14,15,16,17,18,19,20,21,22];
  timeOptionsPaired: Array<number> = [8, 10, 12, 14, 16, 18, 20];
  dayOptions: Array<any> = [
    { text: 'Maanantai', value: 'mon' },
    { text: 'Tiistai', value: 'tue' },
    { text: 'Keskiviikko', value: 'wed' },
    { text: 'Torstai', value: 'thu' },
    { text: 'Perjantai', value: 'fri' },
    { text: 'Lauantai', value: 'sat' },
    { text: 'Sunnuntai', value: 'sun' },
  ];
  private saunaActivated: Boolean = false;
  private maintenanceActivated: Boolean = false;
  private saunaDays: Array<String>;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateServicesComponent>,
    @Inject(MAT_DIALOG_DATA) private property
  ) {
  }

  ngOnInit() {
    this.updateServicesForm = this.formBuilder.group({
      sauna: [this.property.sauna],
      saunaStart: [{
        value: this.property.saunaStart,
        disabled: !this.property.sauna
      }],
      saunaEnd: [{
        value: this.property.saunaEnd,
        disabled: !this.property.sauna}],
      saunaDays: [{
        value: this.property.saunaDays,
        disabled: !this.property.sauna
      }],
      laundry: [this.property.laundry],
      laundryStart: [{
        value: this.property.laundryStart,
        disabled: !this.property.laundry
      }],
      laundryEnd: [{
        value: this.property.laundryEnd,
        disabled: !this.property.laundry}],
      laundryDays: [{
        value: this.property.laundryDays,
        disabled: !this.property.laundry
      }],
      maintenance: [this.property.maintenance]
    })
  }

  handleSaunaChange(value) {
    const saunaStart = this.updateServicesForm.get('saunaStart');
    const saunaEnd = this.updateServicesForm.get('saunaEnd');
    const saunaDays = this.updateServicesForm.get('saunaDays');

    if(value.checked == true) {
      saunaStart.enable();
      saunaEnd.enable();
      saunaDays.enable();
    } else if(value.checked == false) {
      saunaStart.disable();
      saunaEnd.disable();
      saunaDays.disable();
    }
  }

  handleLaundryChange(value) {
    const laundryStart = this.updateServicesForm.get('laundryStart');
    const laundryEnd = this.updateServicesForm.get('laundryEnd');
    const laundryDays = this.updateServicesForm.get('laundryDays');

    if(value.checked == true) {
      laundryStart.enable();
      laundryEnd.enable();
      laundryDays.enable();
    } else if(value.checked == false) {
      laundryStart.disable();
      laundryEnd.disable();
      laundryDays.disable();
    }
  }

  confirmUpdate(form) {
    this.dialogRef.close(form.value);
  }

}
