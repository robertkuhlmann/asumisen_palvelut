import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { UpdateServicesComponent } from './update-services/update-services.component';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin-services',
  templateUrl: './admin-services.component.html',
  styleUrls: ['./admin-services.component.css']
})
export class AdminServicesComponent implements OnInit {

  @Input('properties') properties: Array<any>;
  private selectedProperty: any;
  private loading: Boolean = false;
  updateServicesDialogRef: MatDialogRef<UpdateServicesComponent>;

  constructor(
    private db: AngularFirestore,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  selectProperty(event) {
    const property = event.value;
    this.loading = true;
    this.selectedProperty = property;
  }

  openUpdateServicesForm(property) {
    this.updateServicesDialogRef = this.dialog.open(UpdateServicesComponent, {
      data: property
    });

    this.updateServicesDialogRef
      .afterClosed()
      .subscribe(formData => {
        if(formData && typeof formData !== 'undefined') {
          this.db.collection('properties')
            .doc(property.id)
            .update(formData)
            .then(() => {
              this.snackBar.open("Kiinteistön tiedot päivitetty.", "", {
                duration: 5000
              });
            }, (err) => {
              console.log(err);
              this.snackBar.open(
                "Kiinteistön tietojen päivittäminen epäonnistui.",
                "",
                { duration: 5000 }
              );
            });
          this.selectedProperty = null;
        }
      });
  }

}
