import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.css']
})
export class AdminMenuComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  navigateTo(adminRoute) {
    if(!adminRoute) {
      this.router.navigate(['/admin']);
    } else {
      this.router.navigate([`/admin/${adminRoute}`]);
    }
  }

}
