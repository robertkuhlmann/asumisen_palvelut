import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-init',
  templateUrl: './admin-init.component.html',
  styleUrls: ['./admin-init.component.css']
})
export class AdminInitComponent implements OnInit {

  /* Käyttäjän id (generoidaan automaattisesti Googlella kirjautuvalle käyttäjälle) */
  @Input('userID') userID: string;

  /* Lomakkeen muttuja, sekä lomakkeen lähetyspainiketta hallitseva muuttuja */
  initForm: FormGroup;
  submitted: Boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    private snackBar: MatSnackBar,
    private router: Router,
    private auth: AngularFireAuth
  ) {}

  ngOnInit() {
    /* Alustetaan lomake */
    this.initForm = this.formBuilder.group({
      maintenanceName: [
        null,
        [Validators.required]
      ],
      maintenanceEmail: [
        null,
        [Validators.required, Validators.email]
      ],
      maintenanceNumber: [
        null,
        [Validators.required, Validators.pattern(/^[\+0-9][0-9]*[ ]*[0-9]*$/)]
      ]
    });
  }

  /* Lomakkeen lähetys */
  async onSubmit() {
    this.submitted = true;
    let data = this.initForm.value;

    try {
      /* Tallennetaan aluksi kiinteistöhuolto */
      const savedRecord = await this.db.collection('property-maintenance').add(data);
      
      /* Lähetetään virhe-ilmoitus, jos tallennus epäonnistuu */
      if(!savedRecord) {
        throw new Error("Kiinteistöhuollon luomisessa tapahtui ongelma.");
      }

      /* Luodaan pohja users-tauluun tallennettavalle dokumentille */
      const userData = {
        maintenanceId: savedRecord.id // Vain kiinteistöhuollon id tallennetaan käyttäjälle
      };

      /* Uuden käyttäjän tallennus users-tauluun */
      await this.db.collection('users')
        .doc(this.userID)
        .set(userData);

      /* Ilmoitus käyttäjälle onnistuneesta tallennuksesta */
      this.snackBar.open("Uusi kiinteistöhuolto alustettu.", "", {
        duration: 10000
      });

    } catch(e) {
      /* Virhe-ilmoituksen lähetys */
      console.log(e.message);
      this.snackBar.open(e.message, "", {
        duration: 10000
      });
    }
  }
}
