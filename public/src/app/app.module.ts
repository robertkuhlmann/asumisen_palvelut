import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';

// environment
import { environment } from '../environments/environment';

// Angular Fire
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

// Services
import { AuthService } from './providers/auth.service';

// Materials
import { MatButtonModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatChipsModule } from '@angular/material/chips';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { PropertyInfoComponent } from './service-page/components/property-info/property-info.component';
import { ServiceListComponent } from './service-page/components/service-list/service-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MaintenanceFormComponent } from './service-page/components/maintenance-form/maintenance-form.component';
import { AdminComponent } from './admin/admin.component';
import { AdminMenuComponent } from './admin/components/admin-menu/admin-menu.component';
import { AdminSettingsComponent } from './admin/components/admin-settings/admin-settings.component';
import { AdminPropertiesComponent } from './admin/components/admin-properties/admin-properties.component';
import { AdminUsersComponent } from './admin/components/admin-users/admin-users.component';
import { AdminServicesComponent } from './admin/components/admin-services/admin-services.component';
import { PropertyNewComponent } from './admin/components/admin-properties/property-new/property-new.component';
import { RemovePropertyComponent } from './admin/components/admin-properties/remove-property/remove-property.component';
import { UpdatePropertyComponent } from './admin/components/admin-properties/update-property/update-property.component';
import { SaunaFormComponent } from './service-page/components/sauna-form/sauna-form.component';
import { AdminServiceRequestsComponent } from './admin/components/admin-service-requests/admin-service-requests.component';
import { AdminInitComponent } from './admin/components/admin-init/admin-init.component';

import { QRCodeModule } from 'angular2-qrcode';
import { InfoFeedComponent } from './service-page/components/info-feed/info-feed.component';
import { AdminInfoFeedComponent } from './admin/components/admin-info-feed/admin-info-feed.component';
import { PostNewComponent } from './admin/components/admin-info-feed/post-new/post-new.component';
import { PostUpdateComponent } from './admin/components/admin-info-feed/post-update/post-update.component';
import { PostRemoveComponent } from './admin/components/admin-info-feed/post-remove/post-remove.component';
import { UpdateServicesComponent } from './admin/components/admin-services/update-services/update-services.component';
import { InstructionNewComponent } from './admin/components/admin-info-feed/instruction-new/instruction-new.component';
import { InstructionRemoveComponent } from './admin/components/admin-info-feed/instruction-remove/instruction-remove.component';
import { LaundryFormComponent } from './service-page/components/laundry-form/laundry-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicePageComponent,
    PropertyInfoComponent,
    ServiceListComponent,
    NavbarComponent,
    MaintenanceFormComponent,
    AdminComponent,
    MaintenanceFormComponent,
    AdminMenuComponent,
    AdminSettingsComponent,
    AdminPropertiesComponent,
    AdminUsersComponent,
    AdminServicesComponent,
    PropertyNewComponent,
    RemovePropertyComponent,
    UpdatePropertyComponent,
    SaunaFormComponent,
    AdminServiceRequestsComponent,
    AdminInitComponent,
    InfoFeedComponent,
    AdminInfoFeedComponent,
    PostNewComponent,
    PostUpdateComponent,
    PostRemoveComponent,
    UpdateServicesComponent,
    InstructionNewComponent,
    InstructionRemoveComponent,
    LaundryFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googlemaps
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    QRCodeModule
  ],
  exports: [
    MatButtonModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    RemovePropertyComponent,
    UpdatePropertyComponent,
    PostNewComponent,
    PostRemoveComponent,
    PostUpdateComponent,
    UpdateServicesComponent,
    InstructionNewComponent,
    InstructionRemoveComponent
  ]
})
export class AppModule { }
