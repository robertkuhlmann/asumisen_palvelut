const functions = require('firebase-functions');

// Firestore Database
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const firestore = admin.firestore();

// Validate and follow serviceRequests via an HTTPS url.
exports.validateServiceRequest = functions.https.onRequest((req, res) => {
  const document = req.query.id;
  let serviceRequest;
  let property;
  let initialStatus = 'Odottaa jatkokäsittelyä.';
  let validationMessage = '';
  return firestore.collection('serviceRequest').doc(document).get()
  .then((x) => {
      serviceRequest = x.data();

      // validate if request is unvalidated
      if(!serviceRequest.validated){
          return firestore.collection('serviceRequest').doc(document).update({validated: true, status: initialStatus});
      } else{
          return Promise.resolve(false);
      }
  })
  .then((x) => {

      // show validation message if needed, and fill in .status.
      if(x){
          validationMessage = 'Palvelupyyntö varmennettu onnistuneesti!';
          serviceRequest.status = initialStatus;
      } else if (!serviceRequest.status){
          serviceRequest.status = initialStatus;
      }

      // get property info
      return firestore.collection('properties').doc(serviceRequest.propertyId).get();
  })
  .then((x) => {
      property = x.data();

      // build and serve simple HTML.
      let html = '';
      if(validationMessage){
        html += `<h1>${validationMessage}</h1>`;
      }
      html += `
        <p><strong>Nimi:</strong> ${serviceRequest.tenantName}</p>
        <p><strong>Email:</strong> ${serviceRequest.tenantEmail}</p>
        <p><strong>Puhelin:</strong> ${serviceRequest.tenantPhone}</p>

        <p><strong>Osoite:</strong> ${property.address} ${property.city} ${property.postalCode}</p>
        <p><strong>Kuvaus:</strong><p>
        <p>${serviceRequest.description}</p>
        `;
      if(serviceRequest.picture){
        html += `<img height="200" src="${serviceRequest.picture}">`;
      }
      html += `<p><strong>Tila:</strong> ${serviceRequest.status}</p>`;
      
      res.send(html);
      return true;
  })
  .catch((err) => {
      console.error(err);
      res.status(500).send('VIRHE palvelupyynnön käsittelyssä.');
      return false;     
  });
});

// Delete expired documents via an HTTPS url.
exports.deleteExpiredDocuments = functions.https.onRequest((req, res) => {
  let maxAge = 3;
  let batch = firestore.batch();
  return firestore.collection('serviceRequest').get()
  .then((querySnapshot) => {
      let currentTime = Date.parse(new Date());
  
      for(let document of querySnapshot.docs){
  
          let creationTime = Date.parse(document.createTime);
          let documentAge = (currentTime - creationTime)/(1000*60*60*24);
  
          if(!document.data().validated && documentAge > maxAge){
              try{
                batch.delete(document.ref);
              }
              catch(e){
                  console.error(e);
              }
          }
      }
      batch.commit()
      .then((x) => {
        res.send('ok');
        return true;
      });
  });
});
