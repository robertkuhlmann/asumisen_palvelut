# Firebase Functions #

Cloud Functions used by the project.

- `firebase login`
- `firebase use [PROJECT_ID]`    (check with 'firebase list')
- `cd functions && npm install`
- `firebase deploy --only functions`