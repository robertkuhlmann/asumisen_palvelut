const admin = require('firebase-admin');
const serviceAccount = require('../credentials/firestore.json');

let db = null;

function init() {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });
  db = admin.firestore();
}
exports.init = init;

// Hae dokumentteja
async function getDocuments(collection) {
  try {
    const snapshot = await db.collection(collection).get();
    let arr = [];
    for(let doc of snapshot.docs) {
      arr.push(doc.data());
    }
    return arr;
  } catch(err) {
    throw err;
  }

}
exports.getDocuments = getDocuments;