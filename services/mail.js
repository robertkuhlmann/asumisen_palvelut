const sgMail = require('@sendgrid/mail');
const sendgrid_apikey = require('../credentials/sendgrid.json').api_key;
sgMail.setApiKey(sendgrid_apikey);
const validationLink = process.env.validationLink || 'https://us-central1-asumisen-palvelut.cloudfunctions.net/validateServiceRequest';

function send(info){
  return new Promise((resolve,reject) => {
    console.log(`sending email to ${info.tenantEmail}`);
    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs

    const msg = {
        to: info.tenantEmail,
        from: 'info@asumisenpalvelut2018',
        subject: 'Palvelupyyntö vastaanotettu.',
        text:
        `
        Klikkaa alla olevaa linkkiä varmentaaksesi palvelupyyntö.
        ${validationLink}?id=${info.serviceRequestID}
        `
    };

    sgMail.send(msg, (err,data) => {
      if(err) throw err;
      if(data[0].toJSON().statusCode != 202){
          console.error(err);
          reject(err);
      }
      console.log('ok');
      resolve('ok');
    });
  });

}
exports.send = send;

function sendSauna(data){
  return new Promise((resolve,reject) => {
    console.log(`sending email to ${data.tenantEmail}`);
    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs

    let saunaTime = new Date(data.saunaDay);
    console.log(saunaTime);
    saunaTime.setHours(data.saunaStart);
    const msg = {
        to: data.tenantEmail,
        from: 'info@asumisenpalvelut2018',
        subject: 'Saunavaraus vastaanotettu.',
        text:
        `Saunavuorosi alkaa ${saunaTime}`
    };

    sgMail.send(msg, (err,data) => {
      if(err) throw err;
      if(data[0].toJSON().statusCode != 202){
          console.error(err);
          reject(err);
      }
      console.log('ok');
      resolve('ok');
    });
  });
}
exports.sendSauna = sendSauna;

function sendLaundry(data){
  return new Promise((resolve,reject) => {
    console.log(`sending email to ${data.tenantEmail}`);
    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs

    let laundryTime = new Date(data.laundryDay);
    laundryTime.setHours(data.laundryStart);
    const msg = {
        to: data.tenantEmail,
        from: 'info@asumisenpalvelut2018',
        subject: 'Pyykkivaraus vastaanotettu.',
        text:
        `Pyykkivuorosi alkaa ${laundryTime}`
    };

    sgMail.send(msg, (err,data) => {
      if(err) throw err;
      if(data[0].toJSON().statusCode != 202){
          console.error(err);
          reject(err);
      }
      console.log('ok');
      resolve('ok');
    });
  });
}
exports.sendLaundry = sendLaundry;